(defsystem "typereader"
  :name "typereader"
  :author "GrammaTech"
  :licence "MIT"
  :description "Read type information from Argot sketches."
  :long-description "A utility to collect various forms of type and type-like
annotations from Argot sketches into a unified representation for use in
synthesis modules."
  :depends-on (:typereader/typereader)
  :components ((:file "lisp")
               (:file "typereader"
                :depends-on ("lisp"))
               (:file "tests"
                :depends-on ("typereader")))
  :class :package-inferred-system
  :build-operation "asdf:program-op"
  :build-pathname "typereader"
  :entry-point "typereader/server::run-server"
  :perform
  (test-op (o c) (symbol-call :typereader '#:test)))
