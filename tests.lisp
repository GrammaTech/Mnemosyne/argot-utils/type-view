;;;; Main test package.

;; Manual expansion of `fiasco:define-test-package' to replace the CL import
(defpackage :typereader/test
  (:nicknames :test-typereader)
  (:use :fiasco :typereader :gt/full)
  (:import-from :typereader
   :make-funty :new-refty :mash :funty-params :refty-refinement)
  (:import-from :software-evolution-library/software/lisp
   :from-string :lisp :expression)
  (:import-from :software-evolution-library
   :genome)
  (:import-from :functional-trees
   :children)
  (:import-from :hash-set
   :hs-memberp)
  (:import-from :asdf
   :system-relative-pathname))
(in-package :typereader/test)
(defsuite
    (FIASCO-SUITES::typereader/test :BIND-TO-PACKAGE :typereader/TEST
                                    :IN FIASCO-SUITES::ALL-TESTS))
(in-readtable :curry-compose-reader-macros)

(defun mr (name base)
  "An abbreviation for `make-refty', using `new-refty'."
  (new-refty :name name :base base))

(defparameter ll0 '(a &optional (b 3) &rest x &key c (d a))
  "A lambda list from Section 5.2.2 of CLtL2 using all four kinds of
parameters.")

(defparameter fts0
  '(function (t &optional number &rest t &key (:c t) (:d t)) t)
  "A possible type specifier for a function with `ll0' for a lambda list.")

(defparameter mashed0
  (list (mr 'a t) '&optional `(,(mr 'b 'number) 3) '&rest (mr 'x 'list)
        '&key (mr 'c t) `(,(mr 'd 't) a))
  "The expected output of `funty-params' applied to the result of calling `mash'
  on `ll0' and `ft0'.")

(defparameter mashed01
  (list (mr 'a t) '&optional `(,(mr 'b t) 3) '&rest (mr 'x 'list)
        '&key (mr 'c t) `(,(mr 'd t) a))
  "The expected output of `funty-params' applied to the result of calling `mash'
on `ll0' with no function type specifier (which is to say, the second argument
is null).")

(defparameter ll1
  '(str dims &rest keyword-pairs &key (start 0) end &allow-other-keys)
  "A lambda list from Section 5.2.2 of CLtL2 using &allow-other-keys.")

(defparameter fts1
  '(function
    (string fixnum &rest t &key (:start fixnum) (:end fixnum) &allow-other-keys)
    t)
  "A possible type specifier for a function with `ll1' for a lambda list.")

(defparameter mashed1
  (list (mr 'str 'string) (mr 'dims 'fixnum)
        '&rest (mr 'keyword-pairs 'list)
        '&key `(,(mr 'start 'fixnum) 0) (mr 'end 'fixnum) '&allow-other-keys)
  "The expected output of `funty-params' applied to the result of calling `mash'
on `ll1' and `ft1'.")

(defparameter ll2
  '(&key ((:something-other-than-a a) 0) (b 0 b-supplied-p) (c (+ a b)))
  "A lambda list adapted from Chapter 5 of PCL using an svar (as CLtL calls it)
and specifying a non-default keyword name.")

(defparameter fts2
  '(function (&key (:b number) (:something-other-than-a number) (:c number))
    (values number &optional number &rest number))
  "A possible type specifier for a function with `ll2' for a lambda list,
specifying the keyword parameters' types in a different order and doing weird
stuff in the value type specifier (though that last won't be relevant until
later).")

(defparameter mashed2
  `(&key ((:something-other-than-a ,(mr 'a 'number)) 0)
         (,(mr 'b 'number) 0 b-supplied-p)
         (,(mr 'c 'number) (+ a b)))
  "The expected output of `funty-params' applied to the result of calling `mash'
on `ll2' and `ft2'.")

(defparameter ll3
  '(a &aux (b a) (c :d)))

(defparameter mashed3
  `(,(mr 'a t))
  "The expected output of `funty-params' applied to the result of calling `mash'
on `ll3' with no function type specifier.")

(defparameter ll4
  '(a &rest x))

(defparameter fts4
  '(function (number &rest number) number)
  "A possible type specifier for a function with `ll4' for a lambda list.")

(defparameter mashed4
  (list (mr 'a 'number) '&rest (mr 'x '(:list-of number)))
  "The expected output of `funty-params' applied to the result of calling `mash'
on `ll4' and `fts4'.")

(defparameter testsubstrate0
   "(declaim (ftype (function (integer real string) list) foo bar))
    (proclaim '(ftype (function (integer real string) list) baz))
    (defun foo (x y z)
      ?)
    (defun bar (dog cat chicken)
      (+ 17 ?))
    (defun baz (x y z)
      (append (foo ? ? ?) ?))")

(defparameter params-foo-baz
  (list (mr 'x 'integer) (mr 'y 'real) (mr 'z 'string)))

(defparameter params-bar
  (list (mr 'dog 'integer) (mr 'cat 'real) (mr 'chicken 'string)))

(defparameter testsubstrate1
   "(declaim (ftype (function (t t) t) foo))
    (defun foo (x y)
      \"This documentation string should be ignored.\"
      (declare (type (byte 8) x y))   ; from CLtL2 p. 225
      (+ x y))")

(defparameter testsubstrate2
  "(-> foo (integer integer) integer)
   (defun foo (x y)
     (assert (< x y) (y))
     (- y x))")

(defparameter new-type-symbols-substrate
  "(defstruct foo-struct \"Foo!\" x y z)
   (defstruct (bar-struct (:constructor elephant)) \"Bar!\" a b c)
   (defclass foo-class () (one two three) (:documentation \"Foo again!\"))
   (deftype foo ())
   (deftype bar (&optional x y z)
     `(array (integer x) (y z)))
   (define-condition mint () (:report 'report))
   (defstruct (baz-struct :copier :predicate (:type list)) fee fi fo fum)
   (defun funfunfun (a b c d e f g)
     (declare (foo-struct a) (bar-struct b) (foo-class c) (foo d) (bar e)
              (mint f) (baz-struct g) (list g)))"
  "For testing the functions that add new type names.")

;; It would be a good idea to make sure the :results slots look OK, but that
;; involves working around `gensym's and can probably be deferred to later.

;; Here I use `equalp' because string cases won't make a difference and I want
;; to compare structures' structures.
(deftest mash-test-0 () (is (equalp (funty-params (mash ll0 fts0)) mashed0)))

(deftest mash-test-01 () (is (equalp (funty-params (mash ll0 nil)) mashed01)))

(deftest mash-test-1 () (is (equalp (funty-params (mash ll1 fts1)) mashed1)))

(deftest mash-test-2 () (is (equalp (funty-params (mash ll2 fts2)) mashed2)))

(deftest mash-test-3 () (is (equalp (funty-params (mash ll3 nil)) mashed3)))

(deftest mash-test-4 () (is (equalp (funty-params (mash ll4 fts4)) mashed4)))

(deftest declaim-defun-info-in-table ()
  (let* ((h (read-types 'lisp testsubstrate0))
         (type-info-foo (gethash 'foo h))
         (type-info-bar (gethash 'bar h))
         (type-info-baz (gethash 'baz h)))
    (is (and (typep type-info-foo 'typereader::funty)
             (typep type-info-bar 'typereader::funty)
             (typep type-info-baz 'typereader::funty)
             (equalp (typereader::funty-params type-info-foo) params-foo-baz)
             (equalp (typereader::funty-params type-info-bar) params-bar)
             (equalp (typereader::funty-params type-info-baz)
                     params-foo-baz)))))

(deftest typespec-detection-test-0 ()
  "Do new atomic type specifier names get detected?"
  (let* ((h (read-types 'lisp new-type-symbols-substrate))
         (funfunfunty-params (funty-params (gethash 'funfunfun h))))
    (is (and (every (lambda (x y) (member x (refty-refinement y) :test 'equal))
                    (mapcar {list 'typep}
                            '(a b c d e f)
                            '(foo-struct bar-struct foo-class foo bar mint))
                    funfunfunty-params)
             (not (member '(typep g baz-struct)
                          (refty-refinement (car (last funfunfunty-params)))
                          :test 'equal))))))

(deftest on-self ()
  "As of now, this test doesn't care about what's in the result; it just wants
to make sure that the program works on some decent-sized files as a sanity check
because TypeReader shouldn't fail on any valid Lisp file."
  (is (every (lambda (p)
               (the hash-table
                    (read-types
                     'lisp (system-relative-pathname 'typereader p))))
             '("lisp.lisp" "typereader.lisp" "tests.lisp"))))

;; TODO: Write more comprehensive tests.
