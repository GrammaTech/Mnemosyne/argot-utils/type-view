;;;; TypeReader code specifically for Lisp-based Argot.

(in-package :typereader)
(in-readtable :curry-compose-reader-macros)

(defstruct (function-location (:conc-name fun-loc-))
  "Hold information about the source locations of the definition and type
proclamation of a function.

Instances get created for use in an `intermediate' structure and passed through
to the `funty' structure synthesized therefrom."
  ;; This is, I think, not appropriate for generic functions because one may
  ;; also need to track associated `defmethod's.
  (definition nil :type (or lisp-ast null))
  (proclamation nil :type (or lisp-ast null)))

(defmethod print-object ((object function-location) stream)
  "Hide the details of the location.

The rationale here is that one can inspect the slots individually if one wants
to see them; always printing them just adds clutter."
  (print-unreadable-object (object stream :type t :identity t)))

(defstruct intermediate
  "Hold intermediate, unconsolidated information about a global function.

LOCATION holds information about the locations of the function and its
proclaimed type.
LAMBDA-LIST is the function's lambda list.
TYPESPEC is the function type specifier proclaimed to apply to the function."
  (location nil :type (or null function-location))
  (lambda-list nil :type list)
  (typespec nil :type list))

(defstruct refty
  "Implicitly represent a refinement type (i.e., a base type refined with a
Boolean formula) as a single list of conjuncts involving NAME that inhabitants
of the type must satisfy.

One can think of REFINEMENT as implicitly having the atom `and' at its head.

When used in the parameter list of a `funty', NAME should be the parameter's
name.

RAW-REFINEMENT-INFO contains essentially the same information as REFINEMENT, but
(as the name suggests) in a \"rawer\" format, namely a list of nodes from the
AST of the source under analysis."
  (name (gensym) :type symbol)
  (refinement () :type list)
  (raw-refinement-info nil :type list))

(defmethod print-object ((object refty) stream
                         &aux (name (refty-name object))
                           (refinement (reverse (refty-refinement object)))
                           (base-container? (car refinement)))
  "Print a refinement type in a nice-ish format."
  ;; The following compromises between simplicity and avoiding repeated
  ;; computations; it could move in either direction, and I highly doubt that it
  ;; is optimal as it is now.
  ;; And if you can find a clever way to rewrite this with `match', by all
  ;; means, go ahead.
  (let* ((car-list-p (listp base-container?))
         (typep? (and car-list-p (first base-container?)))
         (name? (and car-list-p (second base-container?)))
         (type? (and car-list-p (third base-container?)))
         (car-contains-base-p (and (eq typep? 'typep)
                                    (eq name? name)))
         (base (if car-contains-base-p type? t))
         (rest (if car-contains-base-p (cdr refinement) refinement)))
       (format stream "{~A\:~A | ~A}" name base (cons 'and rest))))

(-> new-refty (&key (:name symbol) (:base (or symbol list))) refty)
(defun new-refty (&key name base &aux (rt (make-refty)))
  "Make a new refinement type named NAME and based on the Lisp type specifier
BASE."
  (flet ((nonredundantp (base)
           "Is BASE nonredundant, i.e., does it contribute useful information?"
           (and base (not (eq base t)))))
    (if name (setf (refty-name rt) name))
    (if (nonredundantp base)
        (push `(typep ,(refty-name rt) ,base) (refty-refinement rt)))
    rt))

(defstruct funty
  "Represent the type of a function with lists of refinement types for both
the parameters and the results.

The parameter list is essentially a lambda list, with refinement types replacing
parameter names and in which refinement formulae may refer to other parameters.

The MISC slot contains miscellaneous information pertaining to the function that
may or may not have been integrated into the parameter or result lists."
  (location nil :type (or null function-location))
  (params nil :type list)
  (results nil :type list))

;; TODO
;; (defmethod print-object ((object funty) stream))

(defvar *atomic-type-specifiers*)

(defun basic-typespec-set ()
  "Return the default set of atomic type specifiers, i.e. symbols naming classes
plus atomic type specifiers not associated with classes.

To be called before analyzing a new AST, in case multiple independent ASTs get
analyzed in the same session."
  (list-to-hs
   (union
    (filter (lambda (sym) (find-class sym nil))
            (package-exports :cl))
    ;; TODO (maybe): Get symbols from packages actually used in the source under
    ;; analysis rather than assuming CL (though one could argue that if you're
    ;; using funky type specifiers, then you should just use the word TYPE in
    ;; your declarations).
    '(atom
      base-char
      bit
      compiled-function
      extended-char
      keyword
      long-float
      nil
      short-float
      signed-byte
      standard-char
      standard-generic-function
      unsigned-byte))))

(defun typespecp (object)
  "Is OBJECT (supposed to be) a type specifier, given that it appears at the
head of a declaration specifier?

That is, is object an atomic type specifier or a list (assumed to be a compound
type specifier)?

This function doesn't check for errors such as (vector 42 'foo nil \"bar\")."
  (or (and (atom object) (hs-memberp *atomic-type-specifiers* object))
      (listp object)))

(-> adding-function-types (list list hash-table lisp-ast) t)
(defun adding-function-types (names function-type-specifier h node)
  "Add FUNCTION-TYPE-SPECIFIER to the `intermediate' structure in H for each
function name in NAMES. Also add NODE as the location."
  (dolist (name names)
    (let ((val (gethash name h)))
      (if val
          (setf (intermediate-typespec val) function-type-specifier
                (fun-loc-proclamation (intermediate-location val)) node)
          (setf (gethash name h)             
                (make-intermediate
                 :typespec function-type-specifier
                 :location (make-function-location :proclamation node)))))))

(-> add-declaim-info ((or lisp-ast list) hash-table &optional lisp-ast) t)
(defgeneric add-declaim-info (node/form h &optional node-kludge)
  (:documentation "Assuming NODE/FORM represents a `declaim' form, if it
concerns a function then shove the enclosed type specifier into the
`intermediate' structures in the corresponding entries in H.

The generic function thing is a bit of a kludge to make it easier to write
ADD-ARROW-INFO.")
  (:method ((node lisp-ast) h &optional node-kludge)
    (assert (null node-kludge))   ; don't provide the optional argument here
    (add-declaim-info (expression node) h node))
  (:method ((form list) h &optional node-kludge)
    (dolist (declaration (cdr form))
      (match declaration
        ((list* 'ftype fun-type-spec names)
         (adding-function-types names fun-type-spec h node-kludge))))))

(-> add-proclaim-info (lisp-ast hash-table) t)
(defun add-proclaim-info (node h)
  "Assuming NODE represents a `proclaim' expression, add any function type
information it contains to the `intermediate' structures in the corresponding
entries in H."
  (match (expression node)
    ((list 'proclaim (list 'quote (list* 'ftype fun-type-spec names)))
     (adding-function-types names fun-type-spec h node))))

(-> add-arrow-info (lisp-ast hash-table) t)
(defun add-arrow-info (node h)
  "Assuming NODE represents a `->' form, add the function type information to
the `intermediate' structure in the corresponding entry in H."
  (add-declaim-info (macroexpand-1 (expression node)) h node))

(-> adding-function-info (symbol hash-table list lisp-ast) t)
(defun adding-function-info (name h lambda-list node
                             &aux (val (gethash name h)))
  (if val
      (setf (intermediate-lambda-list val) lambda-list
            (fun-loc-definition (intermediate-location val)) node)
      (setf val (setf (gethash name h)
                      (make-intermediate
                       :lambda-list lambda-list
                       :location (make-function-location
                                  :definition node))))))

(-> add-defun-info (lisp-ast hash-table) t)
(defun add-defun-info (node h)
  "Assuming NODE represents a `defun' form, add its lambda list and location to
the appropriate entry in H, deferring traversal of the body to later."
  (match (expression node)
    ((list* 'defun name ll _)
     (adding-function-info name h ll node))))

(-> add-defgeneric-info (lisp-ast hash-table) t)
(defun add-defgeneric-info (node h)
  "Assuming NODE represents a `defgeneric' form, add its lambda list and 
location to the appropriate entry in H."
  (match (expression node)
    ((list* 'defgeneric name ll _)
     (adding-function-info name h ll node))))

(-> add-defmethod-info (lisp-ast hash-table) t)
(defun add-defmethod-info (node h)
  "Assuming NODE represents a `defmethod' form, add type information contained
therein to the appropriate entry in H."
  ;; TODO: Implement this function for real.
  (match (expression node)
    ((list* 'defmethod function-name _)
     (gethash function-name h))))

(-> add-defstruct-info (lisp-ast) (or hash-set null))
(defun add-defstruct-info (node &aux (name-and-options (cadr (expression node)))
                                  (just-name-p (atom name-and-options))
                                  (structure-name (if just-name-p
                                                      name-and-options
                                                      (car name-and-options))))
  "Assuming NODE represents a `defstruct' form, add the new type name it defines
to `*atomic-type-specifiers*' (ignoring it if the :type specifier is given)."
  (labels
      ((no-type-option-p (options)
         (cond
           ((null options) t)
           ((eq (car-safe (car options)) :type) nil)
           (t (no-type-option-p (cdr options))))))
    (when (or just-name-p (no-type-option-p (cdr name-and-options)))
      (hs-ninsert *atomic-type-specifiers* structure-name))))

(-> add-defclass/type/condition-info (lisp-ast) hash-set)
(defun add-defclass/type/condition-info
  (node &aux (name (cadr (expression node))))
  "Assuming NODE represents a `defclass', `deftype', or `defcondition' form, add
the new type name it defines to `*atomic-type-specifiers*'."
  (hs-ninsert *atomic-type-specifiers* name))

(-> mash (list list) (values funty list))
(defun mash (lambda-list fun-ty-spec)
  "Mash together LAMBDA-LIST and FUN-TY-SPEC into a `funty', returning that
together with a list of the `refty' structures comprising it for later
convenience.

As the names suggest, the first argument should be an ordinary lambda list; the
second should be a function type specifier, though it will be treated (thanks to
the logic in `new-refty') as specifying `t' for every parameter and result if
it's null."
  (let ((params (second fun-ty-spec))
        (result (third fun-ty-spec))
        (param-types ()))
    ;; A chain of inner functions to handle all the parameter possibilities.
    ;; This assumes, without checking, that the given lambda list and function
    ;; type specifier are compatible.    
    ;; One might suggest that this use `alexandria:parse-ordinary-lambda-list',
    ;; but then it would still have to parse the type specifier, and the code
    ;; for the Alexandria function is even longer than the following (and it
    ;; doesn't look much more efficient)…
    (labels
        ((reqing (l p acc &aux (carl (car l)))   ;; get required parameter info
           (case carl
             ((nil &aux) acc)
             (&optional (opting (cdr l) (cdr p) (cons '&optional acc)))
             (&rest (rsting (cdr l) (cdr p) (cons '&rest acc)))
             (&key (keying (cdr l) (cdr p) (cons '&key acc)))
             (t (let ((param (new-refty :name carl :base (car p))))
                  (push param param-types)
                  (reqing (cdr l) (cdr p) (cons param acc))))))
         (opting (l p acc &aux (carl (car l)))   ;; get optional parameter info
           (case carl
             ((nil &aux) acc)
             (&rest (rsting (cdr l) (cdr p) (cons '&rest acc)))
             (&key (keying (cdr l) (cdr p) (cons '&key acc)))
             (t (let* ((atom-carl-p (atom carl))
                       (param (if atom-carl-p
                                  (new-refty :name carl :base (car p))
                                  (new-refty :name (car carl) :base (car p)))))
                  (push param param-types)
                  (if atom-carl-p
                      (opting (cdr l) (cdr p) (cons param acc))
                      (opting (cdr l) (cdr p)
                              (cons `(,param ,@(cdr carl)) acc)))))))
         (rsting (l p acc &aux (carl (car l)))  ;; get rest parameter info
           (let* ((carp (car p))
                  (rt (new-refty
                       :name carl
                       :base
                       ;; The following assumes no one is brave enough to use
                       ;; "nil" in a function type specifier parameter list.
                       (if (member carp '(t nil)) 'list `(:list-of ,carp)))))
             (push rt param-types)
             (case (cadr l)
               ((nil &aux) (cons rt acc))
               (&key (keying (cddr l) (cddr p) `(&key ,rt ,@acc))))))
         (keying (l p acc &aux (carl (car l)))   ;; get keyword parameter info
           ;; Only keyword parameters present in the lambda list get through,
           ;; even if &allow-other-keys is present and the type specifier
           ;; mentions others.
           (case carl
             ((nil &aux) acc)
             (&allow-other-keys
              (keying (cdr l) (memq '&allow-other-keys p)
                      (cons '&allow-other-keys acc)))
             (t
              ;; Would a `macrolet' be more trouble than it'd be worth?
              (let ((caarl (car-safe carl)))
                (cond
                  ((not caarl)   ; CARL is like <var>
                   ;; The corresponding type specifier lambda list entry
                   ;; may not be at the head, so we must pluck it from its
                   ;; place no matter where it lurks.
                   (multiple-value-bind (these those)   ; a.k.a. (sheep goats)
                       (partition
                        (lambda (x)
                          (and (consp x)
                               (eq (intern (symbol-name carl) :keyword)
                                   (car x))))
                        p)
                     (let ((param (new-refty :name carl :base (cadar these))))
                       (keying (cdr l) those (cons param acc)))))
                  ((atom caarl)   ; CARL is like (<var> …)
                   (multiple-value-bind (these those)
                       (partition
                        (lambda (x)
                          (and (consp x)
                               (eq (intern (symbol-name caarl) :keyword)
                                   (car x))))
                        p)
                     (let ((param (new-refty :name caarl :base (cadar these))))
                       (keying (cdr l) those (cons `(,param ,@(cdar l)) acc)))))
                  (t   ; CARL is like ((<keyword> <var>) …)
                   (multiple-value-bind (these those)
                       (partition
                        (lambda (x) (and (consp x) (eq (car caarl) (car x)))) p)
                     (let ((param (new-refty :name (cadr caarl)
                                             :base (cadar these))))
                       (keying (cdr l) those
                               (cons `((,(car caarl) ,param) ,@(cdr carl))
                                     acc)))))))))))
      (declare (optimize (debug 0)))
      (values
       (make-funty
        :params (reverse (reqing lambda-list params ()))
        :results (mapcar
                  #'(lambda (b)
                      (if (member b '(&optional &rest &allow-other-keys))
                          b
                          (new-refty :base b)))
                  (if (consp result) (cdr result) (list result))))
       param-types))))

(declaim (inline push-info-node))
(-> push-info-node (t lisp-ast t hash-table) list)
(defun push-info-node (info node key h)
  "Implement the pattern common to functions like `add-declare-info' of pushing
a cons cell onto a list in a hash table."
  (push (cons info node) (gethash key h)))

(-> add-declare-info (lisp-ast hash-table) t)
(defun add-declare-info (node h)
  "Assuming NODE represents a declaration, add any type information it contains
to the appropriate symbols' entries in H."
  (dolist (decl (cdr (expression node)))
    (let ((decl-head (car decl)))
      (cond
        ((eq decl-head 'type)
         (let ((declared-type (cadr decl)))
           (dolist (var (cddr decl))
             (push-info-node `(typep ,var ,declared-type) node var h))))
        ((typespecp decl-head)
         (dolist (var (cdr decl))
           (push-info-node `(typep ,var ,decl-head) node var h)))))))

(-> add-the-info (lisp-ast hash-table) t)
(defun add-the-info (node h)
  "Assuming NODE represents a `the' form, add any information it provides
about variables' types to the corresponding entries in H."
  ;; This could get ugly.
  ;; One could argue that you should use declarations or assertions instead for
  ;; everything that might be relevant here. But SHOULD one argue that?
  ;; TODO: Make this function do something more useful.
  ;; Hmm, there's nothing to keep someone from using `the' inside `assert'…
  ;; In my opinion, no one really ought to do that, but it's within the
  ;; proverbial realm of possibilities.
  (match (expression node)
    ((list value-type form)
     (push-info-node `(typep ,form ,value-type) node form h))))

(-> add-assert-info (lisp-ast hash-table) t)
(defun add-assert-info (node h)
  "Assuming NODE represents an `assert' form, extract type information from it
and add that information to the appropriate entries in H."
  ;; This is a temporary shoddy implementation using only the specified places,
  ;; though perhaps that should be part of the final implementation to make
  ;; things easier…but then again, we'll still have to look through the test
  ;; form to make sure there are no unbound variables.
  (match (expression node)
    ((list* 'assert test-form places _)
     (dolist (place places)
       (push-info-node test-form node place h)))))

(-> add-check-type-info (lisp-ast hash-table) t)
(defun add-check-type-info (node h)
  "Assuming NODE represents a `check-type' form, add the typespec to the entry
for the place in H."
  (match (expression node)
    ((list* 'check-type place typespec)
     (push-info-node `(typep ,place ,typespec) node place h))))

(-> unzip-values (hash-table) hash-table)
(defun unzip-values (h)
  "Replace the value (an a-list) in each entry in H with its unzipping, i.e. a
pair of lists holding the keys and data separately."
  (do-hash-table (k v h h)
    (setf (gethash k h) (funcall «cons {mapcar 'car} {mapcar 'cdr}» v))))

(-> traverse-defun-body (node) hash-table)
(defun traverse-defun-body (defun-node)
  "Traverse the body of DEFUN-NODE and construct refinements for its parameters
based on type information therein.

The resulting hash table associates parameter names with paired lists of
refinements and lists of nodes giving rise to those refinements."
  (let ((h (make-hash-table)))
    (labels
        ((inner (node)
           (case (compound-form-p node)
             (declare (add-declare-info node h))
             (the (add-the-info node h))
             (assert (add-assert-info node h))
             (check-type (add-check-type-info node h))
             (t (mapc #'inner (children node))))))
      (inner defun-node))
    (unzip-values h)))

(-> traverse-defgeneric-body (lisp-ast) hash-table)
(defun traverse-defgeneric-body (defgeneric-node)
  "Traverse the body of DEFGENERIC-NODE and construct refinements for its
parameters based on type information therein, returning a hash table like the
result of `traverse-defun-body'."
  ;; TODO: Look for local information as in `traverse-defun-body' (probably just
  ;; in method descriptions), replacing this placeholder definition that avoids
  ;; STYLE-WARNINGs complaining that parameters aren't used.
  (and defgeneric-node (make-hash-table)))

(-> body-traversal-dispatch (lisp-ast) hash-table)
(defun body-traversal-dispatch (node)
  "Dispatch to the traversal function appropriate for NODE."
  (funcall
   (case (car (expression node))
     (defun 'traverse-defun-body)
     ;; (defmethod 'traverse-defmethod-body)?
     (defgeneric 'traverse-defgeneric-body))
   node))

(-> refine (funty list hash-table) funty)
(defun refine (ft param-types raw-refinements)
  "Refine FT using refinements cooked up from RAW-REFINEMENTS by modifying
elements of PARAM-TYPES, each of which is a `refty' structure from the
`funty-params' slot of FT."
  ;; TODO: Refine results, not just parameters (this will require upstream
  ;; changes, obviously…and also more changes to this function's signature, I
  ;; expect).
  ;; If this ends up getting lists of both the parameter and result refinement
  ;; types and all the information it needs to augment those, then maybe there
  ;; will be no point in it getting passed the whole `funty' if the calling
  ;; context can survive without having it returned. And surely it can.
  ;; (Maybe it can't, and anyway, don't call me Shirley.)
  ;; As you can see, nothing is certain around here. The only constant is
  ;; change. Waste is a thief, people. We need to monetize our assets to achive
  ;; synergy: make that your primary action item.
  ;; Can I get the syntax highlighting in cornflower blue?
  (dolist (param-type param-types)
    (let* ((pair (gethash (refty-name param-type) raw-refinements)))
      (setf (refty-refinement param-type)
            ;; Not the best solution…
            (append (car pair) (refty-refinement param-type))
            (refty-raw-refinement-info param-type)
            (cdr pair))))
  ft)

(-> consolidate (hash-table) hash-table)
(defun consolidate (h)
  "Consolidate the intermediate type information in each entry in H into a nicer
`funty' representation.

This function modifies its argument and then returns it for convenience."
  ;; I fear that "consolidate" is no longer a good name for this function.
  (do-hash-table (k v h h)
    (let* ((loc (intermediate-location v))
           (partial
             (multiple-value-call 'refine
               (mash (intermediate-lambda-list v) (intermediate-typespec v))
               (body-traversal-dispatch (fun-loc-definition loc)))))
      (setf (funty-location partial) loc
            (gethash k h) partial))))

(defmethod extract-type-info ((source lisp-ast)
                              &aux (h (make-hash-table))
                                (*atomic-type-specifiers* (basic-typespec-set)))
  "Return a hash table associating each function name used in a `defun' form in
SOURCE with a `funty' structure built from the function's lambda list and
information from relevant declarations and other constructs.

This could use `ft:mapc', but that would be mildly wasteful because some
subtrees would get processed both here and in functions invoked here, and
context is important, so some kind of extra state would be needed to have a
single function that does everything for all kinds of nodes."
  (labels
      ((inner (node)
         "Inner function to traverse the tree rooted at NODE, adding
pertinent stuff to H."
         (case (compound-form-p node)
           (defun (add-defun-info node h))
           (defgeneric (add-defgeneric-info node h))
           (defmethod (add-defmethod-info node h))
           (declaim (add-declaim-info node h))
           (-> (add-arrow-info node h))
           (proclaim (add-proclaim-info node h))
           (defstruct (add-defstruct-info node))
           ((defclass deftype define-condition)
            (add-defclass/type/condition-info node))
           ;; TODO (maybe): Make sure imported/used packages' type symbols also
           ;; get added to the set of atomic type specifiers.
           (t (mapc #'inner (children node))))))
    (inner source)
    (consolidate h)))
