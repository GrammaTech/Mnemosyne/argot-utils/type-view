;;;; General TypeReader code common to all base languages.

(defpackage :typereader/typereader
  (:nicknames :typereader)
  (:use :gt/full
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/lisp
        :software-evolution-library/software/javascript
        :software-evolution-library/software/python
        :hash-set :fiasco :command-line-arguments)
  (:export :read-types))
(in-package :typereader)
(in-readtable :curry-compose-reader-macros)

(declaim (inline probably-path-p))
(-> probably-path-p (string) (or null pathname))
(defun probably-path-p (possible-path)
  "If POSSIBLE-PATH probably denotes a path, return it.

For now, \"probably denotes a path\" just means \"is only one line\"."
  (unless (find #\Newline possible-path)
    (pathname possible-path)))

(-> extract-type-info (functional-tree-ast) hash-table)
(defgeneric extract-type-info (source)
  (:documentation "Return a hash table associating global function names with
relevant type information."))

(defgeneric read-types (lang prog)
  (:documentation "Read and return type information in the program PROG, assumed
to be written in the language LANG.

PROG may be represented as a string containing the program, a pathname object
indicating the location of a file containing the program, an SEL software
object, or an AST.

The language must be specified explicitly until someone defines a programmatic
way of determining the base language from an Argot specification.")
  (:method (lang (tree functional-tree-ast))
    (extract-type-info tree))
  (:method (lang (obj software))
    (read-types lang (genome obj)))
  (:method (lang (text string))
    (read-types lang (or (probably-path-p text)
                         (from-string (make-instance lang) text))))
  (:method (lang (path pathname))
    (read-types lang (from-file (make-instance lang) path))))


;;;; External command-line driver.
;;;
;;; Compile with the following command:
;;;   sbcl --eval '(ql:quickload :typereader)' \
;;;        --eval '(asdf:make :typereader :type :program :monolithic t)'
;;;
(define-command server ()
  "" "" nil)
