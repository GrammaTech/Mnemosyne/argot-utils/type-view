TypeReader
==========

A utility to collect various forms of type and type-like annotations from Argot
sketches into a unified representation for Muses to use.

<!-- "Type views that a Muse can use"? -->

> NOTE: This project is very new and not yet fit for use.

> ANOTHER NOTE: This module was originally called "TypeReader". I haven't
> changed occurrences of that name in this document because the name could
> change again for all I know. When a final name has been chosen, someone can go
> through and substitute it for "TypeReader" and make sure that doesn't disrupt
> the flow of any sentences.

## Abstract

<!-- "We don't use h1 headings, as they already are displayed on every page as
its title, and we should not apply more than one h1 per page." But the same page
also says "It is a best practice to write each a [sic] paragraph on a single
line." So I'm just picking and choosing things arbitrarily from the style guide
at this point. -->

A synthesis Muse that traffics in types must extract the relevant information
from Argot specifications, dealing with the variety of ways said information may
be expressed. It seems logical to pull that functionality out of individual
synthesis modules and implement it once to avoid duplicating work. This module
is an experiment in such an implementation, aiming to provide a consolidated
representation of the type information expressed in Argot.

<!-- Is "Argot specification" still a reasonable term, if it ever was? What
about "Argot sketch"? "Argot program"? "Argot file"? -->

The objective of this module is to get all the "type" (in the sense of
"universal constraint" as used around the Mnemosyne documentation; never mind
that "tests" are really just a special kind of refinement type using
implication, so the distinction between those categories is artificial)
information that might be useful for Muses so the Muses don't have to worry
about collecting it, and the idea in extracting information from native base
language constructs as much as possible is to allow developers to give synthesis
tools informative type information with which to work without having to learn
about refinement types, and Argot's (to-be-defined) language for expressing
them, explicitly.

## API

This section explains how to interact with TypeReader and how to interpret its
output. It assumes knowledge of [SEL](https://github.com/grammatech/sel) (as
does the next section).

The generic function `read-types` constitutes the public interface to
TypeReader; given an Argot specification (as a file represented by a `pathname`
object, a string containing the program text, an SEL software object, or the AST
underlying a software object) `prog` and the base language `lang` used in the
specification, `(read-types lang prog)` evaluates to an object containing type
information extracted from the program.  The exact nature of this object depends
on the base language, as discussed below.

At present, TypeReader returns only information regarding global functions,
which may be left partly undefined with the symbol `?` representing a "hole" in
the function body (as in
[Idris](http://docs.idris-lang.org/en/latest/tutorial/typesfuns.html#holes)) to
be filled later by programmers and/or Muses, since functions are where most of
the action is likely to happen in program synthesis.

### Common Lisp

Given an Argot specification using Common Lisp as the base language, TypeReader
produces a hash table associating the name of each function fully or partially
defined in the file with a structure containing (theoretically) all available
information concerning that function's type.

Specifically, suppose the input file contains a form beginning with `defun foo`,
with a lambda list following `foo`. Then the output hash table will contain an
entry with key `foo` and the following value.

```lisp
#S(FUNTY :LOCATION <location> :PARAMS <params> :RESULTS <results>)
```

Here, `<location>` is a `function-location` structure specifying the locations
of the function's definition and type proclamation. The function
`fun-loc-definition` applied to such a structure gets you the AST node
containing the relevant function definition; `fun-loc-proclamation` gets you the
node (if any) containing the type proclaimed to apply to the function.
Meanwhile, `<params>` and `<results>` are both lists containing `refty`
structures representing refinement types to which the corresponding arguments or
results must belong in a correct implementation.

The idea behind a `funty` (short for "function type") structure's parameter and
result lists is to mimic Common Lisp's built-in function type specifiers while
providing more information than those native constructs typically express. The
`params` list replicates the function's lambda list, omitting auxiliary
variables and replacing parameter names with `refty` structures containing those
names, while the `results` list contains types for the values returned by the
function interspersed as appropriate with any keywords in the value type
specifier proclaimed for the function. Note that even in the case of multiple
result values, the symbol `values` does not appear at the head of the list.

The idea behind `refty` (short for "refinement type") structures, instances of
which comprise the aforementioned lists in `funty` slots, is (implicitly) to
augment a native Common Lisp type specifier with a refinement predicate
indicating a subset of the specified type to which the corresponding parameter
or result must belong. For a function parameter, the `name` slot holds the
parameter's name; otherwise, (in particular, in `funty-results` `refty` objects)
it's an uninterned symbol. The `refinement` slot holds a list, the elements
whereof may use the symbol in the `name` slot and indicate things that must be
true of any objects belonging to the type represented by the structure. For
example, if such a list for a `refty` with `name` `x` is `((> x 0) (typep x
fixnum))`, then `1` and `most-positive-fixnum` are members of the type, while
`-1` and `(1+ most-positive-fixnum)` are not. Note that an empty `refinement`
list indicates not the empty type but rather the universal type, i.e. as far as
`refinement` lists are concerned, `()` is equivalent to `((typep x t))`
(assuming the name is `x`). Finally, the `raw-refinement-info` slot holds AST
nodes from which the refinement information was derived.

The language of refinement types as used here is not fully specified as yet, but
the general idea is to base it on Lisp's syntax while departing from native Lisp
constructs where necessary. Right now the primary divergence from regular Lisp
is the use of `(:list-of x)` to denote the type containing lists each of whose
elements `e` satisfies `(typep e x)`. As more decisions like this are made, at
least to specify implications in refinements, they should probably be documented
in their own section.

`refty` structures are printed with something like a standard refinement type
syntax, as used in, e.g.,
[LiquidHaskell](https://ucsd-progsys.github.io/liquidhaskell-tutorial/Tutorial_03_Basic.html).
The applicable method of `print-objects` reverses the `refinement` list and
checks whether the last (now first) element specifies a type for the symbol in
the `name` slot using `typep`, using that type as the base if so and otherwise
using `t`. All other bits of the reversed list, i.e. the whole list if no base
was extracted from the head and the tail of the list otherwise, are printed as
the type's refinement in the tail of a list with `and` at the head; this is
meant to be viewed roughly as a Boolean formula that must evaluate to true for
members of the type (though based on Lisp's syntax, it's not necessarily limited
to it, as discussed in the previous paragraph). This takes advantage of the fact
that an empty conjunction is defined to be true. So a `refty` with `name` `Y`
and `refinement` `((< X Y) (TYPEP Y INTEGER))` would be printed as
`{Y:INTEGER | (AND (< X Y))}` (where presumably `X` names another parameter of
the same function, if this occurs in a `funty-params` list.

Some examples might make all of this clearer.

Consider first the tests for the function `mash`. Don't worry about what that
function does right now; just note that `mash-test-0` indicates that a function
with lambda list

```lisp
(A &OPTIONAL (B 3) &REST X &KEY C (D A))
```

and proclaimed (via `declaim`, `proclaim`, or equivalent) type specifier

```lisp
(FUNCTION (T &OPTIONAL NUMBER &REST T &KEY (:C T) (:D T)) T)
```

should get a `funty` with the following `params` list (if no other type
information for the function is known).

```lisp
({A:T | (AND)} &OPTIONAL ({B:NUMBER | (AND)} 3) &REST
 {X:LIST | (AND)} &KEY {C:T | (AND)} ({D:T | (AND)} A))
```

It should also get something like the following `results` list, but that's not
checked by this test right now.

```lisp
({G1337:T | (AND)})
```

The other `mash` tests demonstrate some other cases, illustrating the same
general idea. The test `declaim-defun-info-in-table` illustrates the operation
of TypeReader on actual program text, albeit for a shrimpy program; you can
`(ql:quickload :typereader)` and get into the `typereader/test` package to have
a look at the relevant test package parameters in a REPL.

Keep in mind that each `refty` object also has a `raw-refinement-info` slot, but
it's no fun (and of no use, I believe) to print those.

For an idea of what else refinement slots can do, consider the following
program.

```lisp
(-> foo (integer integer) integer)
(defun foo (x y)
  (assert (< x y) (y))
  (- y x))
```

Here, `foo` should get a type like the following.

```lisp
#S(FUNTY
   :LOCATION #<FUNCTION-LOCATION {10075FA473}>
   :PARAMS ({X:INTEGER | (AND)} {Y:INTEGER | (AND (< X Y))})
   :RESULTS ({G1016:INTEGER | (AND)}))
```

This is but a fleeting taste of the wonders this module has in store for those
who truly believe. For a look behind the scenes at how the proverbial sausage is
made, check out the "Implementation" section below.

### JavaScript

> TBD.

### Python

> TBD.

## Implementation

This section explains how TypeReader works, with the intent of enabling readers
to modify and extend the module, by describing important functions in greater
detail than in the documentation strings in the source. I suppose there's a bit
of redundancy between this section and the previous one; think of it as
providing slightly different perspectives on the same concepts.

For best results, I recommend reading the following concurrently with the
source.

#### `(read-types lang prog)`

This constitutes the module's top-level interface. If `prog` represents an Argot
specification somehow (as a string, `pathname` object, software object, or AST),
the corresponding AST is produced and work is dispatched to
`extract-type-info`.

#### `(extract-type-info source)`

From this point, type extraction proceeds in a manner specific to the base
language. At present, this function is specified as always returning a hash
table, but that may not be the most convenient data structure for the eventual
Python and JavaScript implementations, if indeed those end up sharing the
infrastructure defined here.

### Common Lisp

#### `(extract-type-info source)`

The method specialized for `lisp-ast` objects traverses `source` starting from
the root, at each node either invoking a specialized function (with a name like
`add-defun-info`) to deal with the subtree rooted there or continuing to the
children. A hash table `h` holds collected information; functions for constructs
that contain information about global functions in the Argot being analyzed add
that information to `intermediate` structures (described below) in the values of
entries in `h`, with symbols naming global functions as keys. Functions for
constructs that create new type specifiers don't change `h`; they simply ensure
that the new type specifiers will be recognized as such later.

Traversal of the AST happens in two phases; the first, in this method, does not
look in function bodies but only records the function definitions' locations,
delaying traversal of the bodies until all global information has been
collected. The second phase is initiated by calling `consolidate` on the hash
table full of `intermediate` structures at the end of this method.

#### `(make-function-location &key definition proclamation)`

`function-location` structures are, believe it or not, used to store the
locations of functions, specifically the AST nodes at the roots of the subtrees
where the function is defined (in the `definition` slot) and where its type is
proclaimed (in the `proclamation` slot).

These are added to `intermediate` objects and later transferred to `funty`
objects.

As a comment in the structure definition notes, these structures are designed
for functions defined in `defun` forms; generic functions will need something
more complicated because they may have methods defined separately (perhaps even
in different files, suggesting that perhaps this module should accept several
files constituting an entire project simultaneously).

#### `(make-intermediate &key location lambda-list typespec)`

This function makes `intermediate` structures, of course. Each function with a
global definition gets such a structure associated with its name during the
first phase of AST traversal.

An `intermediate` structure has slots to hold
1. The function's location, as a `function-location` structure;
2. the function's lambda list, probably procured from a `defun` or `defgeneric`
   form; and
3. the type specifier proclaimed (in a `proclaim` expression or `declaim` form,
   or something equivalent like `serapeum:->`) to apply to the function.

These structures exist to deal with the fact that the proclamation of a
function's type may appear before or after the function's definition, or not at
all, and also to delay inspection of the function body until new global type
specifiers have been recorded.

#### `(make-refty &key name refinement raw-refinement-info)`

`refty` structures are described above, under "API". I think there's nothing
more to say about them right here.

#### `(new-refty &key name base)`

In days gone by, a `refty` structure contained a separate `base` slot to store a
Lisp type specifier acting as the type on which the refinement type was based.
This slot was redundant, since a base of `x` for a type with name `n` can be
expressed just as easily by including `(typep n x)` in the refinement list, so
the slot was removed. However, it's still convenient to think in terms of a
refinement type based on a particular type specifier, so `new-refty` provides a
wrapper around the `refty` constructor accommodating this.

A term for the base type only gets added to the refinement list if the `base`
argument actually conveys something useful; if it's not provided or is `t`, then
it's left out. Theoretically, one could say that a parameter's type is `nil`,
but that amounts to saying that no one is allowed to call the function, so I
assume no one will do that. If you really want a type with no inhabitants, then
you'll have to create it manually, without using this function. And you can do
it in any number of ways: if the name is `x`, then you can add `(typep x nil)`
to the list of refinements, but you could also just add `nil` or anything
evaluating thereto (e.g.
`(if "this is a needlessly contrived way to get a contradiction" (not 17))`).

If there ends up being no future use of the default `make-refty` outside of this
function, then perhaps the `:constructor` option should be used to change the
default constructor's name and this should be renamed to `make-refty`.

#### `(make-funty &key location params results)`

`funty` structures are, like `refty`s, described above.

#### `(basic-typespec-set)`

There is a basic set of atomic type specifiers in Common Lisp. This function
returns it (as a hash set). Upon each invocation of `extract-type-info`, the
global variable `*atomic-type-specifiers*` is bound to the result of this
function in case previous invocations in the same Lisp image added new type
specifiers defined in `defclass` or similar forms.

#### `(add-declaim-info node/form h &optional node-kludge)`

If `node/form` says that certain function type specifiers apply to certain
functions, then they get added to the appropriate `intermediate` structures in
`h` by calling `adding-function-types`.

#### `(add-proclaim-info node h)`

This basically does the same thing as `add-declaim-info`, accounting for the
slightly different syntax of a `proclaim` expression.

#### `(adding-function-types names function-type-specifier h node)`

This does the work common to `add-declaim-info` and `add-proclaim-info`, adding
`function-type-specifier` to the `intermediate` structure in `h` for each
function name in `names` (or creating the `intermediate` structure if none
already exists—which will usually be the case, assuming most people declare the
types of their functions before defining them).

#### `(add-arrow-info node h)`

This deals with the `serapeum:->` macro by invoking `add-declaim-info` on the
macroexpansion of the expression in `node`. Some users might import different
packages with alternate definitions for `->`, in which case this could cause
trouble. Eventually provision should be made for that.

#### `(add-defun-info node h)`

This just adds the function's lambda list and location to the `intermediate`
structure under the function's name in `h`, or creates that structure if it's
not already there. Local type information gets integrated into the type
constructed for the function later.

#### `(add-defgeneric-info node h)`

At the moment, this is identical to `add-defun-info` except for the replacement
of the word `defun` with `degeneric` in a few places. It's probably not worth it
to write a macro to generate both of these since the heart of both functions has
already been extracted into `adding-function-info` and they've been left with
onlly three non-documentation lines in their bodies. And generic functions will
need different treatment if they end up not using the same location structures
as regular functions, which is possible.

Actually, though, as things stand now, since checking for the identity of the
form in the `match` expressions in these functions is unnecessary given that
they'll only be called when appropriate, we could just use the same function to
add information for both `defun` and `degeneric` forms, replacing
`'defun`/`'defgeneric` with `_` in the `match`es. If these functions remain
identical, then I think someone should do that at some point.

#### `(adding-function-info name lambda-list h node)`

Analogously to `adding-function-types`, this function performs tasks necessary
in both `add-defun-info` and `add-defgeneric-info`: making sure the
`intermediate` structures associated with `name` in `h` have `lambda-list` and
`node` in the right places.

#### `(add-defmethod-info node h)`

When a good solution has been found to the problem of information pertinent to
generic functions being spread across more than two locations, this function
will deal with `defmethod` forms.

#### `(add-defstruct-info node)`

This function and `add-defclass/type/condition-info` make sure newly defined
atomic type specifiers get added to `*atomic-type-specifiers*`. That's all there
is to it.

Detecting new condition names is probably useless in practice, but it's included
for the sake of completeness.

#### `(mash lambda-list fun-ty-spec)`

So…this function is terrible. I'm pretty sure it works, and ideally it won't
need modification in the future. [Send me an email](mailto:jschwartz427@ksu.edu)
if you need to know how it works and the code is not sufficiently
self-explanatory. There are some tests that I think pretty effectively
illustrate what it does.

It returns two values: a `funty` and a simple list of pointers to the `refty`s
from the `funty`'s `params` list to avoid having to work around the structure of
the list again. The second return value was added after the function was already
written.

It's called "mash" because you can think of it as mashing a lambda list and a
function type specifier together. I think I got the idea for the name while I
was going to town on some freshly cooked pinto beans with a potato masher.

If I'm not mistaken, all `funty` structures in this program get created here.

#### `(consolidate h)`

For every entry (i.e. pair of function name and `intermediate` structure) in
`h`:

- `mash` the lambda list and type specifier into a `funty`;
- use `traverse-defun-body` or similar to collect local type information in the
  function body into a hash table mapping parameter names to cons cells each
  pairing a list of "refinements" (construed as something that must be true of
  inhabitants of the type being "refined") with a list of nodes pointing to the
  source locations from which said refinements were derived
  + (exactly how to deal with generic functions here needs further
    consideration—one possibility is to combine the body traversal functions
    into one generic function with different methods for `defun`, `defgeneric`,
    and `defmethod` forms, but for now I've just written two different such
    functions, leaving open the possibility of another for `defmethod`s, with
    `body-traversal-dispatch` acting as a wrapper to call the correct one);
- call `refine` on the `mash`ed `funty` (and the plain list of `refty`s that
  `mash` also returns) and hash table of local information
  + (but even when results start getting refined, as a comment notes, it may be
    unnecessary to pass in the `funty` if it's just there to get returned for
    convenience);
- transfer the location structure from the `intermediate` structure to the
  `funty`; and
- replace the `intermediate` structure in `h` with the `funty`.

Then return `h` as the final product of TypeReader, theoretically providing
Muses with all the type information they might possibly want and saving them
from collecting it themselves.

#### `(traverse-defun-body node)`

In short, continue the AST traversal on the body of the `defun` form in `node`.

Reminiscent of `extract-type-info`, this consists of looking at each node and
invoking special functions on some while continuing to the children on the
rest, calling `unzip-values` at the end to get the hash table it returns into
the right format.

The "special functions" invoked here mostly have only basic placeholder
definitions at the moment, and more such functions are probably needed to handle
any other type-expressing constructs that can appear within function
definitions.

#### `(traverse-defgeneric-body node)`

This needs to be implemented, as the comment in the method indicates.

#### `(unzip-values h)`

Functions called from `traverse-defun-body` etc. push onto the value lists in
the hash table they're given cons cells pairing refinements with nodes from the
AST specifying the origin of the refinements. So this function unzips those
value lists into pairs of lists of refinements and lists of nodes giving rise to
those refinements, to be added to the `refinement` and `raw-refinement-info`
lists, respectively, of some `refty` in the `funty` under construction.

[Sam Estep](https://gitlab.com/sestep) suggested the nice concise reader
macro–using thing that does all the work.

#### `(add-declare-info node h)`

This function might actually be complete in its current form. The idea is that
for each declaration specifier in the `declare` form, if it has to do with a
type (as signaled by using the symbol `type` or else a type specifier at its
head), then we want to add that type to the hash table entries for all the
variables to which it applies.

The same general pattern, viz. identifying a piece of type information and
pushing it onto the lists associated in the hash table with the variables to
which it applies, is shared by this function with its siblings:
`add-the-info` etc.

#### `(refine ft param-types raw-refinements)`

As discussed above, the arguments to this function will be the `funty` `ft`
whose parameter and (in the future) result types are getting refined along with
its constituent list (later lists) `param-types` (plus presumably
`result-types`) of `refty`s and a hash table `raw-refinements` associating
things (including parameters) with those unzipped pairs of lists produced by
`unzip-values` at the end of `traverse-defun-body` (or similar functions).

Right now, the list of refinements from the unzipped pair for each parameter
just gets shoved onto the front of the `refinement` list for that parameter's
`refty` (which before this point should only have one element, from `mash`),
while that `refty`'s `raw-refinement-info` slot (which should previously be
empty) is set to the list of nodes from the unzipped pair. It should thus be the
case that the refinements in the `refinement` list and the nodes in the
`raw-refinement-info` list correspond: the first element of the latter should be
the source of the first element of the former, and so on (until you get to the
end; if there's a final element in `refinement` without a partner in the other
list, it should have come from the proclamation applying to the function, and
that location is tracked elsewhere).

As you can tell, this function needs more careful thought.

### JavaScript

> TBD.

### Python

> TBD.

## Miscellaneous notes

For the purposes of this module, it is an error to proclaim the type of a
function more than once, using both `proclaim` and `declaim`, or either one
multiple times, for the same function. Likewise, it is an error to define
multiple global functions with the same name (since only the last definition
would be accessible anyway).

We assume that the name and lambda list are in fact given in a `defun` form. We
will ignore, or at least defer to later, cases of incomplete `defun`s with a
hole for either of those components (they're probably irrelevant).

There's a bit of an oddity with optional and keyword parameters in that you can
say that they must have types like `number` of which `nil` is not a member, but
they'll still get that value if they're not supplied and haven't got default
values. This may or may not really be a problem. I suppose anyone, or
anyprogram, writing a function with such parameters should know better than to
use them without making sure they were supplied or at least are non-null.

The function `consolidate` was named under a different paradigm of type
extraction, and that name is no longer quite as applicable. In summary—the old
model was: traverse the AST collecting information, then consolidate that
information into `funty`s that contain `refty`s; the new model is: traverse some
parts of the tree near the root collecting global information about functions,
saving function bodies for later, then traverse function body subtrees
constructing `refty`s to put in `funty`s. As you can see, the new model is
harder to describe concisely (not that you're not welcome to try to describe it
conciselyer).

There are two different kinds of hash tables involved in this module's
implementation that should not be confused: one hash table associates function
names first with `intermediate` structures and then with `funty` structures and
gets returned at the end; and in the process of constructing those `funty`s, for
each function there is a separate hash table associating parameter (and other
variable, or just thing in general) names with type information about those
parameters.

It'll be easier to define exactly what form this module's results should have
and what operations it should support when there's a better understanding of
what other Muses will want from it. The basic `funty` system used at present
will probably need a lot of modification, but I hope that all this at least
provides a foundation for future work.

Regarding the `print-object` method for `refty`s: here is where it possibly
would have been nice to keep the `base` slot, to make it easier to print
refinement types the way I've chosen to print them, which may or may not end up
being a good idea depending on how closely the printed representation of a
`refty` should correspond to how it actually is.

## Implementation tasks

> The below points pertain to the implementation for Lisp; the implementations
> for JavaScript and Python are at present completely unspecified.

- Handle variants of `typecase` (though that seems more like something to use in
  synthesized code based on given refinement types than something from which to
  derive refinement types) and in general implications in types.
- Add support for annotations from the `cl-annot` package, specially formatted
  comments, or whatever other syntax Mnemosyne ends up providing for expressing
  refinement types directly.
- Consider whether there exists a better definition for `probably-path-p`,
  perhaps including a printed warning like "Warning: Assuming the string is a
  path and not program text" when that assumption is made for a string input to
  `read-types`. But really, how likely is that to be necessary?
- Deal with generic functions, probably adding implicative conjuncts to their
  parameters' and/or results' refinements. And determine how to deal with their
  locations when there may be many associated `defmethod`s.  Probably just
  generally rethink how to handle the differences between generic and ordinary
  functions.
- Refine result types based on information in function bodies.
- Write more unit tests for functions whose behavior is unlikely to change in
  the future.
- Decide how to handle cases like `the` forms inside `assert` forms.
- Write `print-object` methods at least for the structures that are part of the
  program's output (`refty` and `funty`).
- Change the first person singular language used here to first person plural,
  I guess, now that I won't be the only maintainer.

## Questions
- Would it be useful to let users define their own type-expressing syntax? That
  shouldn't be strictly necessary, but conceivably it could be useful.

## Copyright and Acknowledgments

Copyright (C) 2020 GrammaTech, Inc.

This code is licensed under the MIT license. See the LICENSE file in
the project root for license terms.

The project or effort depicted was sponsored by the Air Force Research
Laboratory (AFRL) and the Defense Advanced Research Projects Agency
(DARPA) under contract no. FA8750-15-C-0113. Any opinions, findings,
and conclusions or recommendations expressed in this material are
those of the author(s) and do not necessarily reflect the views of
AFRL or DARPA.
